package com.bartslota.availability

import groovy.transform.CompileStatic
import spock.util.concurrent.PollingConditions

@CompileStatic
class PredefinedPollingConditions {
    static final PollingConditions SHORT_WAIT = new PollingConditions(timeout: timeout(DEFAULT_SHORT))
    static final PollingConditions WAIT = new PollingConditions(timeout: timeout(DEFAULT_MEDIUM))
    static final PollingConditions LONG_WAIT = new PollingConditions(timeout: timeout(DEFAULT_LONG))
    static final PollingConditions SHORT_WAIT_WITH_INITIAL_DELAY = new PollingConditions(timeout: timeout(DEFAULT_SHORT) + 1, initialDelay: timeout(1))

    private static final String MULTIPLIER_PROPERTY = "tests.polling.timeout.multiplier"
    private static final int DEFAULT_SHORT = 3
    private static final int DEFAULT_MEDIUM = 10
    private static final int DEFAULT_LONG = 30

    private static int timeout(int defaultSeconds) {
        return System.getProperty(MULTIPLIER_PROPERTY) == null ? defaultSeconds
                : Integer.valueOf(System.getProperty(MULTIPLIER_PROPERTY)) * defaultSeconds
    }
}
