package com.bartslota.availability.testcontainers

import com.github.dockerjava.api.command.InspectContainerResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testcontainers.containers.Container
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.utility.DockerImageName

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.stream.Collectors

class KafkaContainerWrapper extends KafkaContainer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaContainerWrapper.getName())
    private static final String CREATE_TOPIC_FORMAT = "/usr/bin/kafka-topics --create --if-not-exists --zookeeper localhost:2181 --replication-factor %s --partitions %s --topic %s"
    private static final String SUPPORTED_TOPICS_LABEL = "SUPPORTED_TOPICS_LABEL"
    private static final String SUPPORTED_TOPICS_SEPARATOR = "###"
    static final Map<String, Integer> PARTITION_COUNT_BY_TOPIC_NAME = [
            "asset-availability": 1
    ]

    private static KafkaContainerWrapper CONTAINER
    private static int replicationFactor = 1
    private ExecutorService topicCreationPool
    private List<Future<ExecResult>> topicCreationResults
    private boolean reused

    private KafkaContainerWrapper(DockerImageName kafkaImageName) {
        super(kafkaImageName)
    }

    static KafkaContainer getContainer() {
        if (CONTAINER == null) {
            CONTAINER = new KafkaContainerWrapper(DockerImageName.parse("confluentinc/cp-kafka:5.3.0"))
                    .withReuse(true)
                    .withNetwork(null)
                    .withLabel(SUPPORTED_TOPICS_LABEL, supportedTopics())
            CONTAINER.start()
            CONTAINER.createTopics()
            setupSpringProperties()
        }
        CONTAINER
    }

    static String supportedTopics() {
        PARTITION_COUNT_BY_TOPIC_NAME.keySet().stream().sorted().collect(Collectors.joining(SUPPORTED_TOPICS_SEPARATOR))
    }

    @Override
    protected void containerIsStarting(InspectContainerResponse containerInfo, boolean reused) {
        super.containerIsStarting(containerInfo, reused)
        this.reused = reused;
    }

    @Override
    void stop() {
        super.stop()
    }

    @Override
    void close() {
        super.close()
    }

    void createTopics() {
        if (!reused) {
            setupThreadPool()
            PARTITION_COUNT_BY_TOPIC_NAME.forEach { topicName, partitions -> createTopic(topicName, partitions) }
            waitForTopicsToBeCreated()
            closeThreadPool()
        }
    }

    void setupThreadPool() {
        int maximumThreadCount = dockerProcessorsCount()
        LOG.info("Using pool with {} threads to invoke docker commands", maximumThreadCount)
        topicCreationPool = Executors.newFixedThreadPool(maximumThreadCount)
        topicCreationResults = new ArrayList<>()
    }

    int dockerProcessorsCount() {
        try {
            String commandResult = execInContainer("/bin/sh", "-c", "nproc").getStdout()
            Integer.parseInt(commandResult.replaceAll("\r", "").replaceAll("\n", ""))
        } catch (RuntimeException e) {
            LOG.info("Could not obtain docker processors count due to {}, {}", e.getClass().getSimpleName(), e.getMessage())
            return 1
        }
    }

    void createTopic(String topicName, Integer partitions) {
        String command = String.format(CREATE_TOPIC_FORMAT, replicationFactor, partitions, topicName)
        Future<ExecResult> commandResult = topicCreationPool.submit(new Callable<Container.ExecResult>() {
            @Override
            Container.ExecResult call() throws Exception {
                execInContainer("/bin/sh", "-c", command)
            }
        })
        topicCreationResults.add(commandResult)
    }

    def waitForTopicsToBeCreated() {
        while (!topicCreationResults.isEmpty()) {
            for (Iterator<Future<ExecResult>> resultIterator = topicCreationResults.iterator(); resultIterator.hasNext();) {
                Future<ExecResult> commandResultPromise = resultIterator.next()
                if (!commandResultPromise.isDone()) {
                    continue
                }
                ExecResult commandResult = commandResultPromise.get()
                if (commandResult.getExitCode() != 0) {
                    throw new RuntimeException(String.format("Could not create topic: %s", commandResult.getStdout()))
                }
                LOG.debug(commandResult.getStdout())
                resultIterator.remove()
            }
        }
        LOG.info("All kafka topics successfully created")
    }

    void closeThreadPool() {
        topicCreationPool.shutdown()
    }

    static void setupSpringProperties() {
        String address = kafkaContainerAddress()
        setupBrokerAddress(address)
    }

    private static String kafkaContainerAddress() {
        CONTAINER.getBootstrapServers()
    }

    private static void setupBrokerAddress(String address) {
        System.setProperty("spring.kafka.bootstrapServers", address)
        System.setProperty("kafka.brokers", address)
    }
}
