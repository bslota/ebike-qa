package com.bartslota.availability.testcontainers

import org.springframework.beans.factory.config.BeanPostProcessor

class KafkaContainerConfiguration implements BeanPostProcessor {

    KafkaContainerConfiguration() {
        KafkaContainerWrapper.getContainer()
    }

}