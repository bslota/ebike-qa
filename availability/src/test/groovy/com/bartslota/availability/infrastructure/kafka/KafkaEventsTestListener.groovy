package com.bartslota.availability.infrastructure.kafka

import com.bartslota.availability.events.DomainEvent
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

/**
 * @author bslota on 09/01/2022
 */
@Component
@Slf4j
class KafkaEventsTestListener {

    @Autowired
    private ObjectMapper objectMapper

    private List<DomainEvent> capturedEvents = []

    void cleanup() {
        capturedEvents.clear()
    }

    @KafkaListener(topics = ["asset-availability"])
    void handle(String event) {
        capturedEvents << objectMapper.readValue(event, DomainEvent.class)
        capturedEvents
    }

    boolean containsEventFulfilling(Closure<Boolean> findingClosure) {
        capturedEvents.any {findingClosure(it)}
    }
}
