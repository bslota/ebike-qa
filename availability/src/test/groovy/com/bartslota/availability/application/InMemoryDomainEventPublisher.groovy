package com.bartslota.availability.application

import com.bartslota.availability.events.DomainEvent
import com.bartslota.availability.events.DomainEventsPublisher

/**
 * @author bslota on 23/11/2021
 */
class InMemoryDomainEventPublisher implements DomainEventsPublisher {

    List<DomainEvent> events = []

    @Override
    void publish(DomainEvent domainEvent) {
        events << domainEvent
    }

    void cleanup() {
        events.clear()
    }

    boolean thereWasAnEventFulfilling(Closure<Boolean> condition) {
        events.find(condition).any()
    }
}
