package com.bartslota.availability.application;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bartslota.availability.domain.AssetAvailability;
import com.bartslota.availability.domain.AssetAvailabilityRepository;
import com.bartslota.availability.domain.AssetId;
import com.bartslota.availability.domain.OwnerId;
import com.bartslota.availability.events.AssetActivated;
import com.bartslota.availability.events.AssetActivationRejected;
import com.bartslota.availability.events.AssetLockRejected;
import com.bartslota.availability.events.AssetLocked;
import com.bartslota.availability.events.AssetRegistered;
import com.bartslota.availability.events.AssetRegistrationRejected;
import com.bartslota.availability.events.AssetUnlocked;
import com.bartslota.availability.events.AssetUnlockingRejected;
import com.bartslota.availability.events.AssetWithdrawalRejected;
import com.bartslota.availability.events.AssetWithdrawn;
import com.bartslota.availability.events.DomainEvent;
import com.bartslota.availability.events.DomainEventsPublisher;

import io.micrometer.core.instrument.Tag;
import io.vavr.control.Either;

import static io.micrometer.core.instrument.Metrics.counter;
import static io.micrometer.core.instrument.Metrics.timer;

/**
 * @author bslota on 07/01/2021
 */
@Transactional
@Service
public class AvailabilityService {

    private final AssetAvailabilityRepository repository;
    private final DomainEventsPublisher domainEventsPublisher;

    AvailabilityService(AssetAvailabilityRepository repository, DomainEventsPublisher domainEventsPublisher) {
        this.repository = repository;
        this.domainEventsPublisher = domainEventsPublisher;
    }

    //TODO: create tests for existing asset
    @Secured("ROLE_ADMIN")
    public Either<AssetRegistrationRejected, AssetRegistered> registerAssetWith(AssetId assetId) {
        Optional<AssetAvailability> existingAsset = repository.findBy(assetId);
        if (existingAsset.isEmpty()) {
            repository.save(AssetAvailability.of(assetId));
            AssetRegistered event = AssetRegistered.from(assetId);
            domainEventsPublisher.publish(event);
            return Either.right(event);
        } else {
            return Either.left(AssetRegistrationRejected.dueToAlreadyExistingAssetWith(assetId));
        }
    }

    //TODO: add tests
    @Secured("ROLE_ADMIN")
    public Either<AssetActivationRejected, AssetActivated> activate(AssetId assetId) {
        return repository
                .findBy(assetId)
                .map(asset -> handle(asset, asset.activate()))
                .orElse(Either.left(AssetActivationRejected.dueToMissingAssetWith(assetId)));
    }

    @Secured("ROLE_ADMIN")
    public Either<AssetWithdrawalRejected, AssetWithdrawn> withdraw(AssetId assetId) {
        return repository
                .findBy(assetId)
                .map(asset -> handle(asset, asset.withdraw()))
                .orElse(Either.left(AssetWithdrawalRejected.dueToMissingAssetWith(assetId)));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetLockRejected, AssetLocked> lock(AssetId assetId, OwnerId ownerId, Duration time) {
        return repository
                .findBy(assetId)
                .map(asset -> handle(asset, asset.lockFor(ownerId, time)))
                .orElse(Either.left(AssetLockRejected.dueToMissingAssetWith(assetId, ownerId)))
                .peek(incrementAssetLockedCounterFor(assetId, ownerId));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetLockRejected, AssetLocked> lockIndefinitely(AssetId assetId, OwnerId ownerId) {
        return repository
                .findBy(assetId)
                .map(asset -> handle(asset, asset.lockIndefinitelyFor(ownerId)))
                .orElse(Either.left(AssetLockRejected.dueToMissingAssetWith(assetId, ownerId)));
    }

    @Secured("ROLE_CUSTOMER")
    public Either<AssetUnlockingRejected, AssetUnlocked> unlock(AssetId assetId, OwnerId ownerId, LocalDateTime at) {
        return repository
                .findBy(assetId)
                .map(asset -> handle(asset, asset.unlockFor(ownerId, at)))
                .orElse(Either.left(AssetUnlockingRejected.dueToMissingAssetWith(assetId, ownerId)));
    }

    public void unlockIfOverdue(AssetAvailability assetAvailability) {
        assetAvailability.unlockIfOverdue()
                         .ifPresent(event -> {
                             repository.save(assetAvailability);
                             domainEventsPublisher.publish(event);
                         });
    }

    private <T extends DomainEvent, U extends DomainEvent> Either<T, U> handle(AssetAvailability asset, Either<T, U> executionResult) {
        if (executionResult.isRight()) {
            repository.save(asset);
            domainEventsPublisher.publish(executionResult.get());
        }
        return executionResult;
    }

    private Consumer<AssetLocked> incrementAssetLockedCounterFor(AssetId assetId, OwnerId ownerId) {
        return assetLocked -> counter("asset.locks", List.of(Tag.of("assetId", assetId.asString()), Tag.of("ownerId", ownerId.asString()))).increment();
    }
}
