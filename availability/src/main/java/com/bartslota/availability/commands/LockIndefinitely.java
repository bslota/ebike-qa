package com.bartslota.availability.commands;

/**
 * @author bslota on 25/11/2021
 */
public record LockIndefinitely(String assetId) implements Command {

    static final String TYPE = "LOCK_INDEFINITELY";

    @Override
    public String getType() {
        return TYPE;
    }
}
