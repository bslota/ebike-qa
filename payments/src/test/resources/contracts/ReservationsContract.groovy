package contracts

import org.springframework.cloud.contract.spec.Contract

/**
 * @author bslota on 09/01/2022
 */
Contract.make {
    request {
        method "POST"
        url "/api/accounts/${regex("[a-zA-Z0-9-]*")}/locks"
        body(
                money: $(
                        amount: $(positiveInt()),
                        currency: $(regex("EUR|PLN|GBP"))
                ),
                service: $(alphaNumeric())
        )
        headers {
            contentType('application/json')
        }
    }
    response {
        status CREATED()
        headers {
            header 'Location' : nonEmpty()
        }
    }
}