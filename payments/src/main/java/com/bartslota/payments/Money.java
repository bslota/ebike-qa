package com.bartslota.payments;

import java.math.BigDecimal;

/**
 * @author bslota on 09/01/2022
 */
public record Money(BigDecimal amount, String currency) {

}
