package com.bartslota.c4;

class TeamResponsibilities {

    static void setupTeamResponsibilities(InternalContainers containers) {
        //ALFA
        Teams.TEAM_ALFA.assignTo(containers.reservationsAndLendings);
        Teams.TEAM_ALFA.assignTo(containers.availabilityManagement);

        //BETA
        Teams.TEAM_BETA.assignTo(containers.catalogue);
        Teams.TEAM_BETA.assignTo(containers.stationCommunicator);
        Teams.TEAM_BETA.assignTo(containers.rangeCalculator);

        //GAMMA
        Teams.TEAM_GAMMA.assignTo(containers.priceConfiguration);
        Teams.TEAM_GAMMA.assignTo(containers.payments);
        Teams.TEAM_GAMMA.assignTo(containers.clientsAndUsers);

        //Shared resources
        Teams.SHARED.assignTo(containers.frontend);
    }
}
