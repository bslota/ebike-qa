package com.bartslota.c4;

import java.util.function.Function;

import com.structurizr.Workspace;
import com.structurizr.model.Model;
import com.structurizr.model.SoftwareSystem;
import com.structurizr.view.PaperSize;
import com.structurizr.view.StaticView;
import com.structurizr.view.ViewSet;

import static com.bartslota.c4.CustomTags.EXTERNAL_SYSTEM;
import static com.bartslota.c4.Protocols.REST;
import static com.bartslota.c4.Protocols.SOAP;
import static com.bartslota.c4.ViewCreator.setupView;

class ContextDiagram {

    static ExternalSystems create(Workspace workspace, Model model, SoftwareSystem internalSystem) {
        ExternalSystems externalSystems = new ExternalSystems(model);
        externalSystems.createUsages(internalSystem);
        setupContextView(workspace, internalSystem);
        return externalSystems;
    }

    private static void setupContextView(Workspace workspace, SoftwareSystem externalSystems) {
        Function<ViewSet, StaticView> contextViewCrator = views ->
                views.createSystemContextView(
                        externalSystems,
                        "E-bike context",
                        "E-bike context view");
        setupView(workspace, contextViewCrator, PaperSize.A4_Landscape);
    }
}

class ExternalSystems {

    final SoftwareSystem station;
    final SoftwareSystem onlinePayments;

    ExternalSystems(Model model) {
        station = model.addSoftwareSystem("Bike station", "Embedded system in physical bike stations");
        onlinePayments = model.addSoftwareSystem("Online payments", "Online payments integrator");
        applyTags();
    }

    void createUsages(SoftwareSystem internalSystem) {
        //station
        internalSystem.uses(station, "send vehicle registrations and locks", SOAP);
        station.uses(internalSystem, "send station-related events", SOAP);
        //online payments
        internalSystem.uses(onlinePayments, "send payment requests", REST);
    }

    private void applyTags() {
        station.addTags(EXTERNAL_SYSTEM);
        onlinePayments.addTags(EXTERNAL_SYSTEM);
    }
}
