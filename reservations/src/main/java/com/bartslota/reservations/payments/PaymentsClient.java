package com.bartslota.reservations.payments;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "payments", url = "${payments.api.url}", path = "${payments.api.root-path}")
interface PaymentsClient {

    @PostMapping(value = "/{accountId}/locks", consumes = APPLICATION_JSON_VALUE)
    ResponseEntity<?> send(@PathVariable String accountId, @RequestBody LockRequest request);
}
