package com.bartslota.reservations.payments;

/**
 * @author bslota on 09/01/2022
 */
public record AccountId(String value) {

}
