package com.bartslota.reservations.availability;

/**
 * @author bslota on 05/01/2022
 */
public record UnlockCommand(String assetId) implements Command {

    private static final String TYPE = "UNLOCK";

    @Override
    public String getType() {
        return TYPE;
    }

}
